import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import synergy.challenge.notetaking.database.entity.NoteEntity
import synergy.challenge.notetaking.databinding.InputTextNoteBinding


class InputNoteAdapter(
    private val callback: (NoteEntity) -> Unit,
    private val listener: OnAdapterListener
): RecyclerView.Adapter<InputNoteAdapter.ViewHolder>(){


    private val diffCallback = object : DiffUtil.ItemCallback<NoteEntity>() {
        override fun areItemsTheSame(oldItem: NoteEntity, newItem: NoteEntity): Boolean {
            return oldItem == newItem

        }
        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: NoteEntity, newItem: NoteEntity): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    private var notes: List<NoteEntity> = listOf()


    fun submitData(value: MutableList<NoteEntity>?) = differ.submitList(value)



    class ViewHolder(val binding: InputTextNoteBinding): RecyclerView.ViewHolder(binding.root) {

        companion object{
            fun from(parent: ViewGroup): ViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = InputTextNoteBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }

        fun bind(data: NoteEntity ,callback: (NoteEntity) -> Unit,  listener: OnAdapterListener){
            binding.inputTitleNote.text = data.noteTitle
            binding.inputContentNote.text = data.noteContent
            binding.btnUppdate.setOnClickListener {
                listener.onClick(data)
            }
            binding.btnDelete.setOnClickListener {
                listener.onDelete(data)
            }
        }

    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = InputTextNoteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }



    override fun getItemCount(): Int {
        return differ.currentList.size
    }



    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(differ.currentList[position], callback, listener )
    }



    interface OnAdapterListener{
        fun onClick(note: NoteEntity)
        fun onDelete(note: NoteEntity)
    }


}