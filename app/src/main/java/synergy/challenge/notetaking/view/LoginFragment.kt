package synergy.challenge.notetaking.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import synergy.challenge.notetaking.R
import synergy.challenge.notetaking.ViewModelFactory
import synergy.challenge.notetaking.databinding.FragmentLoginBinding
import synergy.challenge.notetaking.viewModel.UserViewModel

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val userViewModel: UserViewModel by activityViewModels{
        ViewModelFactory.getInstance(requireActivity())
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(layoutInflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textLogin.setOnClickListener {
           userViewModel.getAllUsers().observe(viewLifecycleOwner){
               it.forEach {
                   if (it.username == binding.username.text.toString()){
                       val bundle = Bundle()
                        bundle.putString(USERNAME, it.username)
                       findNavController().navigate(R.id.homePageFragment, bundle)
                   }
               }
           }
        }

        binding.navigateToRegister.setOnClickListener {
            findNavController().navigate(R.id.registerFragment2)
        }


    }


    companion object{
        const val USERNAME = "USERNAME"
    }


}