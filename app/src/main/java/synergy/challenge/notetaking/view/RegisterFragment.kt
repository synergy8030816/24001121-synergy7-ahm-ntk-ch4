package synergy.challenge.notetaking.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import synergy.challenge.notetaking.R
import synergy.challenge.notetaking.ViewModelFactory
import synergy.challenge.notetaking.database.entity.UserEntity
import synergy.challenge.notetaking.databinding.FragmentRegisterBinding
import synergy.challenge.notetaking.viewModel.UserViewModel

class RegisterFragment : Fragment() {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private val viewModel: UserViewModel by activityViewModels {
        ViewModelFactory.getInstance(requireActivity())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonregister.setOnClickListener {

            val user = UserEntity(
                username = binding.inputUsername.text.toString(),
                email = binding.inputEmail.text.toString(),
                password = binding.inputPassword.text.toString()
            )
            viewModel.addUser(user)

            findNavController().navigate(R.id.loginFragment)
        }
    }


}