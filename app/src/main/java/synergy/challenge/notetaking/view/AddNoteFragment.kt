package synergy.challenge.notetaking.view

import android.os.Bundle
import android.text.Editable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import synergy.challenge.notetaking.R
import synergy.challenge.notetaking.ViewModelFactory
import synergy.challenge.notetaking.database.entity.NoteEntity
import synergy.challenge.notetaking.databinding.FragmentAddNoteBinding
import synergy.challenge.notetaking.viewModel.NoteViewModel


class AddNoteFragment : Fragment() {
    private var _binding: FragmentAddNoteBinding? = null
    private val binding get() = _binding!!

    private val noteViewModel: NoteViewModel by activityViewModels {
        ViewModelFactory.getInstance(requireActivity())
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddNoteBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewType = arguments?.getInt(HomePageFragment.KEY_VIEW_TYPE)
        Log.e("KEYVIEW", viewType.toString() )

        when(viewType){
            1 -> {
                setupAddNote()
            }
            2 -> {
                setupEditNote()
            }
            3 -> {
                setupDeleteNote()
            }
        }

    }

    private fun setupDeleteNote() {
        val notes = arguments?.getParcelable<NoteEntity>(HomePageFragment.KEY_BUNDLE_NOTE)

        binding.btnSaveEdit.visibility = View.GONE
        binding.textSaveEdit.visibility = View.GONE
        binding.btnAddNote.visibility = View.GONE
        binding.textAddNote.visibility = View.GONE

        if (notes != null) {
            binding.tittleNote.text = notes.noteTitle?.toEditable()
            binding.contentNote.text = notes.noteContent?.toEditable()
            binding.textDeleteNote.setOnClickListener {
                notes.id?.let {
                    noteViewModel.deleteNote(
                        idNote = it
                    )
                }
                Toast.makeText(requireContext(), "Hapus catatan berhasil", Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.homePageFragment)
            }
            binding.textCancelDelete.setOnClickListener {
                findNavController().navigate(R.id.homePageFragment)
            }
        }
    }

    fun setupAddNote(){
        binding.btnSaveEdit.visibility = View.GONE
        binding.textSaveEdit.visibility = View.GONE

        binding.btnDeleteNote.visibility = View.GONE
        binding.textDeleteNote.visibility = View.GONE
        binding.btnCancelDelete.visibility = View.GONE
        binding.textCancelDelete.visibility = View.GONE
        binding.textAddNote.setOnClickListener {
            val note = NoteEntity(
                noteTitle = binding.tittleNote.text.toString(),
                noteContent = binding.contentNote.text.toString()
            )
            noteViewModel.addNote(note)
            Toast.makeText(requireContext(), "Input catatan berhasil", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.homePageFragment)
        }
    }

    fun setupEditNote(){
        val notes = arguments?.getParcelable<NoteEntity>(HomePageFragment.KEY_BUNDLE_NOTE)

        binding.btnAddNote.visibility = View.GONE
        binding.textAddNote.visibility = View.GONE
        binding.btnDeleteNote.visibility = View.GONE
        binding.textDeleteNote.visibility = View.GONE
        binding.btnCancelDelete.visibility = View.GONE
        binding.textCancelDelete.visibility = View.GONE
        if (notes != null) {
            binding.tittleNote.text = notes.noteTitle?.toEditable()
            binding.contentNote.text = notes.noteContent?.toEditable()
            binding.textSaveEdit.setOnClickListener {
                notes.id?.let {
                    noteViewModel.updateNote(
                        tittleNote =  binding.tittleNote.text.toString(),
                        contentNote = binding.contentNote.text.toString(),
                        idNote = it
                    )
                }
                Toast.makeText(requireContext(), "Update catatan berhasil", Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.homePageFragment)
            }
        }
    }


    fun String.toEditable(): Editable = Editable.Factory.getInstance().newEditable(this)


}