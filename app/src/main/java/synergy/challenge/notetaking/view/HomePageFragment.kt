package synergy.challenge.notetaking.view

import InputNoteAdapter
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import synergy.challenge.notetaking.R
import synergy.challenge.notetaking.ViewModelFactory
import synergy.challenge.notetaking.database.entity.NoteEntity
import synergy.challenge.notetaking.databinding.FragmentHomePageBinding
import synergy.challenge.notetaking.viewModel.NoteViewModel

class HomePageFragment : Fragment(), InputNoteAdapter.OnAdapterListener {
    private var _binding: FragmentHomePageBinding? = null
    private val binding get() = _binding!!

    private val noteViewModel: NoteViewModel by activityViewModels {
        ViewModelFactory.getInstance(requireActivity())
    }

    private lateinit var adapter: InputNoteAdapter



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomePageBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val params = arguments?.getString(LoginFragment.USERNAME)
        binding.username.text = "Welcome $params"

        setupRecyclerView()


        binding.btnAddNote.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(KEY_VIEW_TYPE, ADD_NOTE)
            findNavController().navigate(R.id.addNoteFragment, bundle)
        }

        binding.Logout.setOnClickListener {
            findNavController().navigate(R.id.loginFragment)
        }

    }

    private fun setupRecyclerView() {
        val callback: (NoteEntity) -> Unit = {
        }
        adapter = InputNoteAdapter(callback, this)
        noteViewModel.getNotes().observe(viewLifecycleOwner){
            adapter.submitData(it.toMutableList())
            binding.rvlistBucketNote.adapter = adapter
            binding.rvlistBucketNote.layoutManager = LinearLayoutManager(activity)
        }


    }

    fun bundleEdit(note: NoteEntity, intentType: Int){
        val bundle = Bundle()
        bundle.putParcelable(KEY_BUNDLE_NOTE, note)
        bundle.putInt(KEY_VIEW_TYPE, intentType)
        findNavController().navigate(R.id.addNoteFragment, bundle)

    }


    override fun onClick(note: NoteEntity) {
        bundleEdit(note, EDIT_NOTE)
        Toast.makeText(requireContext(), "override onclick = ${note.id}, ${note.noteTitle}", Toast.LENGTH_SHORT).show()

    }

    override fun onDelete(note: NoteEntity) {
        bundleEdit(note, DELETE_NOTE)
        Toast.makeText(requireContext(), "override DELETE NOTE = ${note.id}, ${note.noteTitle}", Toast.LENGTH_SHORT).show()

    }


    companion object{
        const val KEY_VIEW_TYPE = "KEY_VIEW_TYPE"
        const val KEY_BUNDLE_NOTE = "KEY_BUNDLE_NOTE"
        const val ADD_NOTE = 1
        const val EDIT_NOTE = 2
        const val DELETE_NOTE = 3
    }

}