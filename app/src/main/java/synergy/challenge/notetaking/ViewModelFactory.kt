package synergy.challenge.notetaking

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import synergy.challenge.notetaking.database.Simple_Database
import synergy.challenge.notetaking.database.dao.NoteDao
import synergy.challenge.notetaking.database.dao.UserDao
import synergy.challenge.notetaking.viewModel.NoteViewModel
import synergy.challenge.notetaking.viewModel.UserViewModel

class ViewModelFactory private constructor(
    private val noteDao: NoteDao,
    private val userDao: UserDao
): ViewModelProvider.NewInstanceFactory(){


    companion object{
        @Volatile
        private var instance: ViewModelFactory? = null

        fun getInstance(context: Context): ViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: ViewModelFactory(
                    Simple_Database.getInstance(context).noteDao,
                    Simple_Database.getInstance(context).userDao
                )
            }
    }

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(NoteViewModel::class.java) -> {
                NoteViewModel(noteDao) as T
            } modelClass.isAssignableFrom(UserViewModel::class.java) -> {
                UserViewModel(userDao) as T
            }
            else -> throw Throwable("Unknown ViewModel class: " + modelClass.name)
        }
    }

}