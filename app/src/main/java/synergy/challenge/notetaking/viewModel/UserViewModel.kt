package synergy.challenge.notetaking.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import synergy.challenge.notetaking.database.dao.UserDao
import synergy.challenge.notetaking.database.entity.UserEntity

class UserViewModel(val userDao: UserDao): ViewModel() {
    private val _users: MutableLiveData<List<UserEntity>> = MutableLiveData(emptyList())
    val users get() = _users

    fun addUser(user: UserEntity) {
        userDao.insert(user)
    }
    fun getAllUsers() = userDao.getAllUser()

}