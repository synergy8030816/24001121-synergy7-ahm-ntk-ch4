package synergy.challenge.notetaking.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import synergy.challenge.notetaking.database.dao.NoteDao
import synergy.challenge.notetaking.database.entity.NoteEntity

class NoteViewModel(val noteDao: NoteDao): ViewModel() {
    private val _notes: MutableLiveData<List<NoteEntity>> = MutableLiveData(emptyList())
    val notes get() = _notes

    fun addNote(note: NoteEntity) {
        noteDao.insert(note)
    }

    fun getNotes() = noteDao.getAllNote()

    fun updateNote(tittleNote : String, contentNote: String, idNote : Long){
        noteDao.updateTitleContentNoteById(tittleNote, contentNote, idNote)
    }

//    fun deleteNote(idNote: Long) {
//        val listNote = mutableListOf<NoteEntity>()
//        val dataNote = _notes.value
//        dataNote?.forEach {
//            if (it.id != idNote) {
//                listNote.add(it)
//            }
//        }
//        _notes.value = listNote
//    }

    fun deleteNote(idNote: Long){
        noteDao.deleteById(idNote)
    }
}