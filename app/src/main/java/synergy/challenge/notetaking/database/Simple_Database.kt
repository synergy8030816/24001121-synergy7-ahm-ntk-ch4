package synergy.challenge.notetaking.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import synergy.challenge.notetaking.database.dao.NoteDao
import synergy.challenge.notetaking.database.dao.UserDao
import synergy.challenge.notetaking.database.entity.NoteEntity
import synergy.challenge.notetaking.database.entity.UserEntity


@Database(entities = [UserEntity::class, NoteEntity::class], version = 1, exportSchema = false)
abstract class Simple_Database: RoomDatabase() {

    abstract val userDao : UserDao
    abstract val noteDao : NoteDao

    companion object{

        @Volatile
        private var INSTANCE: Simple_Database? = null

        fun getInstance(context: Context): Simple_Database{
            synchronized(this){
                var instance = INSTANCE

                if (instance == null){
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        Simple_Database::class.java,
                        "simple_database"
                    )
                        .fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }

    }
}