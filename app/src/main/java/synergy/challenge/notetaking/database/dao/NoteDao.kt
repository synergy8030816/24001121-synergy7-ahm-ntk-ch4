package synergy.challenge.notetaking.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import synergy.challenge.notetaking.database.entity.NoteEntity

@Dao
interface NoteDao {
    @Insert
    fun insert(note: NoteEntity)

    @Query("SELECT * FROM table_note ORDER BY id DESC")
    fun getAllNote(): LiveData<List<NoteEntity>>

    @Query("SELECT * FROM table_note WHERE id = :noteId")
    fun getNote(noteId : Long): List<NoteEntity>

    @Delete
    fun delete(note: NoteEntity)

    @Query("DELETE FROM table_note WHERE id = :itemIdParams")
    fun deleteById(itemIdParams: Long)

    @Update
    fun update(note: NoteEntity)

    @Query("UPDATE table_note SET note_title = :newNoteTittle, note_content = :newNoteContent  where id = :noteIdParams")
    fun  updateTitleContentNoteById(newNoteTittle: String, newNoteContent: String, noteIdParams: Long)
}