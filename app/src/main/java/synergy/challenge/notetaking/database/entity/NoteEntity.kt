package synergy.challenge.notetaking.database.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity("table_note")
data class NoteEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,

    @ColumnInfo(name = "note_title")
    var noteTitle: String? = null,

    @ColumnInfo(name = "note_content")
    var noteContent: String? = null
) : Parcelable