package synergy.challenge.notetaking.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import synergy.challenge.notetaking.database.entity.UserEntity

@Dao
interface UserDao {
    @Insert
    fun insert(chart: UserEntity)

    @Query("SELECT * FROM table_user ORDER BY id DESC")
    fun getAllUser(): LiveData<List<UserEntity>>

    @Delete
    fun delete(user: UserEntity)

    @Update
    fun update(chart: UserEntity)

}
